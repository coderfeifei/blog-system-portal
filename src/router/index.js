import Vue from "vue";
import VueRouter from 'vue-router'
import {baseUrl} from "./baseUrl";

//使用组件懒加载的方式

Vue.use(VueRouter)
export default new VueRouter({
    base: baseUrl,
    mode:'history',
    routes:[
        // 开始自动重定向到博客的浏览页面
        {
            path: '/',
            redirect: '/home'
        }
    ]
})