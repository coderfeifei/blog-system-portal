import ajax from './ajax'
import devMenu from '../vuex/devMenu'

// 测试登录接口
export const RetestLogin = (url, data) => ajax('POST', url, data)
// 模拟请求菜单
export const ReqMenu = () => {
    return new Promise((resolve)=>{
        setTimeout(()=>{
            resolve({code:200,data:devMenu})
        },2000)
    })  
}
// 请求天气
export const ReqWeather = (location) => ajax('GET','/weather',{location})