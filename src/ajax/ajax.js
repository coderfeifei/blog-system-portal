import axios from "axios";

const BaseUrl = process.env.NODE_ENV === 'development' ? 'http://localhost:4000' : '/blog_server'
// 封装请求函数
export default (type = 'GET', url, data) => {
    console.log('执行到ajax',type,url,data)
    return new Promise((resolve, reject) => {
        let promise = null
        if (type === 'POST') {
            promise = axios({
                method: type,
                data,
                url: BaseUrl + url
            })
        } else if(type === 'GET') {
            promise = axios({
                method: type,
                params: data,
                url: BaseUrl + url
            })
        }else{
            console.log('参数错误')
            reject('参数错误')
        }
        promise.then(res => {
            resolve(res.data)
        }).catch(err => {
            console.log('err:',err)
            reject('请求失败')
        })
    })
}