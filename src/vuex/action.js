import {GetuserInfo, GetMenuList,GetWeather} from './actionType'
import { ReqMenu} from '../ajax/index'

export  default {
    /* async GetuserInfoAsync({commit},pars){
        //console.log('执行了')
        let {url, data} = pars
        let res = await RetestLogin(url,data)
        if(res.code === 0){
            // 请求成功，通知对应mutation保存用户信息
            commit(GetuserInfo,res.data)
        }else{
            console.log('action.js 请求出问题了',res)
        }
    }, */
    async GetMenuListasync({commit}){
        let res = await ReqMenu()
        if(res.code === 200){
            // 请求成功，通知mutation保存功能菜单
            commit(GetMenuList,res.data)
        }else{
            console.log('action.js 请求出问题了',res)
        }
    },
    // 和子应用的通信方法，注册的时候将这个方法传给子应用
    async GetWeatherAsync({commit},res){
        if(res.code === 0){
            //console.log('这是返回的res', res.data)
            commit(GetWeather,res.data)
        }else{
            console.log('请求天气失败')
        }
    },
    // 和子应用的通信方法，注册的时候将这个方法传给子应用
    GetuserInfoAsync({commit}, res){
        if(res.code === 0){
            commit(GetuserInfo, res.data)
        }else{
            console.log('数据传递失败，检查子应用的状态')
        }
    }
}