// 这里将store暴露出去
import Vuex from 'vuex'
import Vue from 'vue'
import actions from "./action";
import mutations from "./mutation";
import state from "./state";

Vue.use(Vuex)
export default new Vuex.Store({
    state,
    actions,
    mutations
})

