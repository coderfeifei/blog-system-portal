import { baseUrl } from "../router/baseUrl"
// 区分项目的线上部署访问地址和本地开发环境地址
const [entry1, entry2, entry3] = (process.env.NODE_ENV === 'development') 
? ['//localhost:10010', '//localhost:20000', '//localhost:30000']
// 线上地址通过nginx反向代理
: ['/blog_client', '/blogMannage', '/resume']
// console.log('dev',process.env.NODE_ENV)
// 在这里还无法加载到store
/* import store from './index'
console.log('store!!!',store) */
export default [
    // 博客首页对所有用户都开放，博客管理和简历只对登录用户开放
    {
        name: '博客主页',
        entry: entry1,
        container: '#mirco_base',
        activeRule: baseUrl + '/home',
        icon : 'el-icon-s-home',
        route: '/home'
    },
    {
        name: '博客管理',
        entry: entry2,
        container: '#mirco_base',
        activeRule: baseUrl + '/mannage',
        icon : 'el-icon-document',
        route: '/mannage',
        // 传递store 和子应用共享store
        props:{
            /* data:{
                store:store,
                age:20
            } */
        }
    },
    {
        name: '我的简历',
        entry: entry3,
        container: '#mirco_base',
        activeRule: baseUrl + '/resume',
        icon : 'el-icon-s-custom',
        route: '/resume'
    }
]