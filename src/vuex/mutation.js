import {GetuserInfo, GetMenuList,GetWeather} from './actionType'
import { registerMicroApps, start } from 'qiankun';
import store from './index'


export default {
    // 接收到数据进行保存
    [GetuserInfo](state, userInfo){
        state.userInfo = userInfo
    },
    [GetMenuList](state, MenuList){
        state.MenuList = MenuList
        console.log('这是protal的store', store._actions.GetWeatherAsync)
        let mirco = MenuList.map(item => {
            return {
               name:item.name,
               entry:item.entry,
               container: item.container,
               activeRule: item.activeRule,
               // 共享接口
               // eslint-disable-next-line
               props:item.hasOwnProperty('props') ? {'PortalReceiveWeather':store._actions['GetWeatherAsync'][0],
               'PortalReceiveUserInfo':store._actions['GetuserInfoAsync'][0]} : {'store':'没有这个属性'}
            }
        })
        // console.log('mutation.js',mirco)
        // 配置微前端的相关配置
        registerMicroApps(mirco)
        start()
    },
    [GetWeather](state,weather){
        state.weather = weather
    }
}