import Vue from 'vue'
import App from './App.vue'
import store from './vuex/index'
import router from './router/index'
import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';
// import { registerMicroApps, start } from 'qiankun';


Vue.use(Element)
Vue.config.productionTip = false


new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')



