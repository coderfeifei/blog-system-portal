# blog_portal(博客系统门户)
### 分支说明：bug分支用于处理bug,特性分支开发新需求，开发分支是线上部署分支（主分支）。
| 分支名称 | 分支说明 |
| :-----| ----: |
| dev/1.0.0 | 部署（开发）分支 |
|  hotfix| bug分支 |
|  feature| 特性分支 |
|hotfix-share_store|微前端实验分支（测试与基座共享store）|

### 应用集成
集成了博客管理系统和博客客户端
### 开发环境 域名和端口
| 应用名称 | 域名和端口 |
| :-----| ----: |
| protal | 127.0.0.1:9000 |
| blog_clint| 127.0.0.1:10000 |
| blog_mannage| 127.0.0.1:20000 |
| resume| 127.0.0.1:30000 |

### 开发中注意事项
1.如果单独启动子应用的接口是使用子应用自己webpackp配置中的代理。
但是如果微前端方式启动，子应用的访问代理都会走主应用中的代理，所以微前端方式中，要在主应用中配置所有的子应用代理。
2.子应用要实现和主应用的路由联动，子应用的base路由就应该是主应用访问该子应用的激活路径。
### 线上部署方案
部署工具nginx
>项目域名: blog.com

>协议http

> 端口 ： 80（默认端口）
### 部署文档
```
    server {
        listen       80;
        server_name  blog.com;
        # 前端配置
        # portal配置
        location / {
            #windows中 配置nginx地址用/
            root   E:/8-React/reacthooks_blog/blog_portal/portal;
            #其他路径统一走这里
            try_files $uri $uri/ /index.html;
            index  index.html index.htm;
        }
        # blog_client的配置
        location /blog_client{
            root  E:/8-React/reacthooks_blog/blog_client;
            index  index.html index.htm;
        }
        # blogMannage的配置
        location /blogMannage{
            root  E:/8-React/reacthooks_blog/blog_master_vue;
            index  index.html index.htm;
        }
        #后端配置
        # 这里两个都加/的原因就是 如果访问 /blog_server/a -> http://localhost:4000/a
        location /blog_server/{
            proxy_pass  http://localhost:4000/;
        }
    }
```


### 项目部署上线遇到的问题
>1.两个子项目在本地以微前端的方式运行没有问题，部署上线之后，blogMannage无法加载字体资源文件（woff文件），
并且子应用blog_client和blogMannage多次来回切换之后，路由就发生错误了，但是在开发环境是不存在这个问题的。

### 路由切换出现bug，加载进来出问题
<img src = './bug/1.png'>
字体资源先不管，应用加载失败的原因是子应用中使用路由组件的懒加载功能，在使用微前端的部署方式的时候(组件懒加载的时候请求路径错误少了一层/blogMannage)。
<img src = './bug/2.png'>
<img src = './bug/3.png'>
在关闭nginx 中的tryfiles功能之后，路由的懒加载请求就会出现404，可以看到请求的路径少了一层/blogMannage，所有才导致组件懒加载功能出现bug。

```
针对上述问题，下面两个解决方案：
1.取消组件的懒加载（不推荐，性能不好）
2.在懒加载组件的时候加一层路径(这个目前没有找到好的解决方案),在打包好的js文件中手动搜索/static/js，找到动态加载路由的代码，在请求路径上面加一层/blogmannage,这样就能正常打到nginx对应文件。
```

### 微前端中基座和子应用的通信问题，
    这部分在博客管理系统中详细说明（blog_master_vue）